/*
Сделать класс Logger с методом void log(String message),
который выводит в консоль какое-либо сообщение.
Применить паттерн Singleton для Logger.
*/

public class Logger {

    private static final Logger INSTANCE;

    static {
        INSTANCE = new Logger();
    }

    private Logger() { }

    public static Logger getLogger() {
        return INSTANCE;
    }

    public void log(String message) {
        System.out.println(message);
    }
}
