import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    private User parseUser(String line) {
        String[] parts = line.split("\\|");
        return new User(parts[0], Integer.parseInt(parts[1]), Boolean.parseBoolean(parts[2]));
    }

    private List<User> loadUsersFromFile(Predicate<User> predicate) throws IOException {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                User user = parseUser(line);
                if (predicate.test(user))
                    users.add(user);
            }
        }
        return users;
    }

    @Override
    public List<User> findAll() {
        try {
            return loadUsersFromFile(user -> true);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findByAge(int age) {
        try {
            return loadUsersFromFile(user -> user.getAge() == age);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        try {
            return loadUsersFromFile(User::isWorker);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(User user) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
