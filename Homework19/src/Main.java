import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

        System.out.println("== findAll ===============");
        outputIntoConsole(usersRepository.findAll());

        User userIgor = new User("Игорь", 33, true);
        if (!users.contains(userIgor))
            usersRepository.save(userIgor);

        System.out.println("\n== findByAge(33) =========");
        outputIntoConsole(usersRepository.findByAge(22));

        System.out.println("\n== findByIsWorkerIsTrue ==");
        outputIntoConsole(usersRepository.findByIsWorkerIsTrue());
    }

    public static void outputIntoConsole(List<User> users) {
        for (User user : users)
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
    }
}
