/**
 * 11.11.2021
 * 17. LinkedList
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    private Node<T> node(int index) {
        Node<T> node;
        if (index < size / 2) {
            node = first;
            for (int i = 0; i < index; i++)
                node = node.next;
        } else {
            node = last;
            for (int i = size - 1; i > index; i--)
                node = node.prev;
        }
        return node;
    }

    private void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException(String.format("Index %s out of bounds for length %s", index, size));
    }

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        Node<T> prevNode = last;
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        last.prev = prevNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
            first.prev = newNode;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        checkIndex(index);
        return node(index).value;
    }

    public void remove(int index) {
        checkIndex(index);

        Node<T> prevNode = node(index).prev;
        Node<T> nextNode = node(index).next;

        if (prevNode == null)
            first = nextNode;
        else
            prevNode.next = nextNode;

        if (nextNode == null)
            last = prevNode;
        else
            nextNode.prev = prevNode;

        size--;
    }

    @Override
    public String toString() {
        Node<T> node = first;
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (int i = 0; i < size; i++) {
            builder.append(node.value);
            node = node.next;
            if (i < size - 1) builder.append(", ");
        }
        builder.append(']');
        return builder.toString();
    }
}
