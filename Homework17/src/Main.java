/*
На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
Вывести:
Слово - количество раз
Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
Текст для проверки: Мыла Мила мишку мылом, Мила мыло уронила. Уронила Мила мыло, мишку мылом не домыла.
*/

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap();

        System.out.println("Введите строку с текстом:");
        String strLine = new Scanner(System.in).nextLine();

        String[] words = strLine.toLowerCase().split("\\p{IsCommon}");

        for (String word : words)
            if (!word.isEmpty())
                map.put(word, map.getOrDefault(word, 0) + 1);

        System.out.println(map);
    }
}
