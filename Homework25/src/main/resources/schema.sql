/*
 * Create tables
 */
create table product (
      id            serial primary key
    , name          varchar(30) not null 
    , description   varchar(200)
    , price         decimal(10,2)
    , amount        integer 
);

create table customer (
      id            serial primary key
    , name          varchar(40) not null 
);

create table order_of_product (
      id            serial primary key
    , product_id    integer not null
    , customer_id   integer 
    , order_date    date
    , amount        integer 
    , foreign key (product_id) references product (id)
    , foreign key (customer_id) references customer (id)
);


/*
 * Fill tables with data
 */
insert into product (name, description, price, amount) values ('Чашка', 'Кофейная чашка для эспрессо', 100.01, 21);
insert into product (name, description, price, amount) values ('Блюдце', 'Блюдце под кофейную чашку для эспрессо', 150.01, 22);
insert into product (name, description, price, amount) values ('Плоская тарелка', 'Тарелка для вторых блюд', 200.21, 48);
insert into product (name, description, price, amount) values ('Глубокая тарелка', 'Суповая тарелка', 300.41, 34);
insert into product (name, description, price, amount) values ('Графин', 'Графин подходит для подачи различных напитков', 100.01, 15);
insert into product (name, description, price, amount) values ('Супница', 'Изготовлена из доломитовой керамики высокого качества', 100.01, 11);
insert into product (name, description, price, amount) values ('Бокал', 'Бокал для подачи воды', 100.01, 100);

insert into customer (name) values ('Рога и копыта');
insert into customer (name) values ('Коровьев и Ко');
insert into customer (name) values ('Плюшкин');

insert into order_of_product (product_id, customer_id, order_date, amount) values (1,1, to_date('03.03.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (2,1, to_date('03.03.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (3,1, to_date('03.03.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (4,1, to_date('03.03.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (4,2, to_date('01.04.2021','dd.MM.yyyy'), 2);
insert into order_of_product (product_id, customer_id, order_date, amount) values (5,2, to_date('01.04.2021','dd.MM.yyyy'), 1);
insert into order_of_product (product_id, customer_id, order_date, amount) values (6,2, to_date('12.05.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (5,2, to_date('12.05.2021','dd.MM.yyyy'), 1);
insert into order_of_product (product_id, customer_id, order_date, amount) values (4,3, to_date('23.02.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (1,3, to_date('23.02.2021','dd.MM.yyyy'), 6);
insert into order_of_product (product_id, customer_id, order_date, amount) values (2,3, to_date('23.02.2021','dd.MM.yyyy'), 6);


/*
drop table order_of_product;
drop table product;
drop table customer;
*/
