package ru.pcs.nagaevra;

import java.util.List;

public interface ProductsRepository {

    void add(Product p);
    void update(Product p);
    void remove(int id);

    Product findById(int id);

    List<Product> findAll();
    List<Product> findAllByName(String name);
    List<Product> findAllByPrice(double price);
    List<Product> findAllByOrdersCount(int ordersCount);
}
