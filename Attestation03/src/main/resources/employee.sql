/*
drop table employee;
drop table juridical_face;
drop table department;
drop table "position";
*/

create table juridical_face (
    id    bigserial primary key 
  , name  varchar(100)	
);

create table department (
    id    bigserial primary key
  , name  varchar(100)	
);

create table position (
    id    bigserial primary key
  , name  varchar(100)	
);

create table employee (
    id                 bigserial primary key
  , juridical_face_id  bigint
  , department_id      bigint
  , position_id        bigint
  , name               varchar(100)
  , tech_stack         varchar(200)
  , experience         varchar(200)
  , begin_work         date
  , education          varchar(200)
  , dismiss            date
  , dismiss_reason     varchar(200)
  , constraint fk_juridical_face foreign key (juridical_face_id) references juridical_face (id)
  , constraint fk_department foreign key (department_id) references department (id)
  , constraint fk_position foreign key (position_id) references "position" (id)
);


insert into "position" (name) values ('Разработчик');
insert into "position" (name) values ('Аналитик');
insert into "position" (name) values ('Тестировщик');
insert into "position" (name) values ('Ведущий разработчик');
insert into "position" (name) values ('Ведущий аналитик');
insert into "position" (name) values ('Ведущий тестировщик');
insert into "position" (name) values ('Руководитель группы разработчиков');
insert into "position" (name) values ('Руководитель группы аналитиков');
insert into "position" (name) values ('Руководитель группы тестировщиков');
insert into "position" (name) values ('Руководитель проектов');
insert into "position" (name) values ('Руководитель портфеля проектов');


insert into juridical_face (name) values ('ООО "Тим Форс"');
insert into juridical_face (name) values ('ООО "СиДиСи"');
insert into juridical_face (name) values ('ООО "Смартстафинг"');


insert into department (name) values ('Технический департамент');
insert into department (name) values ('Офис управления проектами');
insert into department (name) values ('Департамент развития бизнеса и маркетинга');


insert into employee
(id, juridical_face_id, department_id, position_id, "name", tech_stack, experience, begin_work, education, dismiss, dismiss_reason)
values(1, 3, 1, 20, 'Кожевников Дмитрий Александрович', 'HP Load Runner, Java 8+,  Selenium', 'Опыт автоматизации тестирования с применением  Java\С#, Selenium, Cucumber, Allure', '2017-05-25', 'Высшее 2009, Национальный исследовательский Иркутский государственный технический университет', null, '');
insert into employee
(id, juridical_face_id, department_id, position_id, "name", tech_stack, experience, begin_work, education, dismiss, dismiss_reason)
values(2, 2, 1, 16, 'Степнова Юлия Викторовна', 'SOA, SOAP, MQ, клиент-серверная архитектура, веб-сервисы', 'Опыт применения современных подходов к проектированию и интеграции enterprise-решений', '2018-09-03', 'Высшее, Саровский Физико-Технический институт (МИФИ-4), 2009 г. ', null, '');
insert into employee
(id, juridical_face_id, department_id, position_id, "name", tech_stack, experience, begin_work, education, dismiss, dismiss_reason)
values(3, 3, 2, 21, 'Карасев Александр Викторович', '-', 'Планирование, управление, контроль и координация разработки программного обеспечения', '2017-12-19', 'Высшее, техническое', '2019-06-18', 'Получил офер от конкурирующей компании на гораздо большее ЗП');
insert into employee
(id, juridical_face_id, department_id, position_id, "name", tech_stack, experience, begin_work, education, dismiss, dismiss_reason)
values(4, 2, 1, 18, 'Максимов Виктор Валерьевич', 'Java, Spring MVC, Spring Boot, Hibernate ORM, SQL, Oracle Pl/SQL, PostgreSQL', '31', '2019-07-18', 'Проектирование и разработка ERP-системы для автоматизации предприятия', null, '');
