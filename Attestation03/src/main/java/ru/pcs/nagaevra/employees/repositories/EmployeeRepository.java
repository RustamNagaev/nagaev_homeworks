package ru.pcs.nagaevra.employees.repositories;

import ru.pcs.nagaevra.employees.models.Employee;

import java.util.List;

public interface EmployeeRepository {
    void add(Employee p);
    void update(Employee p);
    void delete(Long id);

    Employee findById(Long id);

    List<Employee> findAll();
    List<Employee> findAllByName(String name);

}
