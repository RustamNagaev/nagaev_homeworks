package ru.pcs.nagaevra.employees.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.nagaevra.employees.models.Department;

import javax.sql.DataSource;
import java.util.List;

@Component
public class DepartmentRepositoryJdbcTemplateImpl implements DepartmentRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select id, name from department order by name";
    private static final String SQL_SELECT_BY_ID = "select id, name from department where id = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DepartmentRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Department findById(Long id) {
        return jdbcTemplate.query(SQL_SELECT_BY_ID, rowMapper, id).get(0);
    }

    @Override
    public List<Department> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, rowMapper);
    }

    private static final RowMapper<Department> rowMapper = (row, rowNum) -> {
        Long id = row.getLong("id");
        String name = row.getString("name");
        return new Department(id, name);
    };

}
