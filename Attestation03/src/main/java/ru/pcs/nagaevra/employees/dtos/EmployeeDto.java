package ru.pcs.nagaevra.employees.dtos;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EmployeeDto {
    private Long id;
    private Long juridicalFaceId;
    private Long departmentId;
    private Long positionId;
    private String name;
    private String techStack;
    private String experience;
    private String beginWork;
    private String education;
    private String dismiss;
    private String dismissReason;
}
