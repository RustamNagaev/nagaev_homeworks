package ru.pcs.nagaevra.employees.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.nagaevra.employees.models.JuridicalFace;

import javax.sql.DataSource;
import java.util.List;

@Component
public class JuridicalFaceRepositoryJdbcTemplateImpl implements JuridicalFaceRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select id, name from juridical_face order by name";
    private static final String SQL_SELECT_BY_ID = "select id, name from juridical_face where id = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public JuridicalFaceRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public JuridicalFace findById(Long id) {
        return jdbcTemplate.query(SQL_SELECT_BY_ID, juridicalFaceRowMapper, id).get(0);
    }

    @Override
    public List<JuridicalFace> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, juridicalFaceRowMapper);
    }

    private static final RowMapper<JuridicalFace> juridicalFaceRowMapper = (row, rowNum) -> {
        Long id = row.getLong("id");
        String name = row.getString("name");
        return new JuridicalFace(id, name);
    };

}