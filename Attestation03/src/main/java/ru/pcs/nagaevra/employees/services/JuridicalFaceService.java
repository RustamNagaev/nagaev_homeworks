package ru.pcs.nagaevra.employees.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.repositories.JuridicalFaceRepository;

import java.util.List;

@Service
public class JuridicalFaceService {

    private final JuridicalFaceRepository juridicalFacesRepository;

    @Autowired
    public JuridicalFaceService(JuridicalFaceRepository juridicalFacesRepository) {
        this.juridicalFacesRepository = juridicalFacesRepository;
    }

    public List<JuridicalFace> findAll() {
        return juridicalFacesRepository.findAll();
    }

}
