package ru.pcs.nagaevra.employees.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.repositories.DepartmentRepository;

import java.util.List;


@Service
public class DepartmentService {

    private final DepartmentRepository departmentsRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentsRepository) {
        this.departmentsRepository = departmentsRepository;
    }

    public List<Department> findAll() {
        return departmentsRepository.findAll();
    }

}
