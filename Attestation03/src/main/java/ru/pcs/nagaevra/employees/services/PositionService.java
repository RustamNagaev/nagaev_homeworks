package ru.pcs.nagaevra.employees.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.repositories.PositionRepository;

import java.util.List;

@Service
public class PositionService {

    private final PositionRepository positionsRepository;

    @Autowired
    public PositionService(PositionRepository positionsRepository) {
        this.positionsRepository = positionsRepository;
    }

    public List<Position> findAll() {
        return positionsRepository.findAll();
    }

}
