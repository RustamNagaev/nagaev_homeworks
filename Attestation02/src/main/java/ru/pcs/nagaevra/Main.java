package ru.pcs.nagaevra;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource(
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "1");

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        outputProductsIntoConsole(productsRepository.findAll(), "Find all product");
        outputProductsIntoConsole(productsRepository.findAllByPrice(100.01), "Find product by price = 100.01");
        outputProductsIntoConsole(productsRepository.findAllByOrdersCount(2), "Find product by order count = 2");

        // to run additional tests, uncomment the line below
        // additionalTests(productsRepository);
    }

    private static void outputProductsIntoConsole(List<Product> products, String desc) {
        String s = "| %5s | %-20s | %10s | %10s |";
        System.out.println("\n" + desc);
        System.out.println("+--------------------------------------------------------+");
        System.out.printf((s) + "%n", "id", "name", "price", "amount");
        System.out.println("|--------------------------------------------------------|");
        s = "| %5s | %-20s | %10.2f | %10s |";
        for (Product product : products) {
            System.out.printf((s) + "%n", product.getId(), product.getName(), product.getPrice(), product.getAmount());
        }
        System.out.println("+--------------------------------------------------------+");
    }

    private static void additionalTests(ProductsRepository productsRepository) {
        List<Product> products = new ArrayList<>();

        // find by id
        products.add(productsRepository.findById(3));
        outputProductsIntoConsole(products, "Find product by id=3");

        // add
        Product product = Product.builder()
                .name("Салатница")
                .description("Салатница стеклянная прозрачная с рельефным узором")
                .price(292.56)
                .amount(5)
                .build();
        productsRepository.add(product);
        products = productsRepository.findAllByName("Салатница");
        outputProductsIntoConsole(products, "Find product by like name = 'Салатница' after add");

        // update
        for (Product p : products) {
            p.setName("Селедочница");
            p.setDescription("Селедочница белая, с рисуноком. Материал фарфор.");
            productsRepository.update(p);
        }
        products = productsRepository.findAllByName("Селедочница");
        outputProductsIntoConsole(products, "Find product by like name = 'Селедочница' after update");

        // remove
        for (Product p : products) {
            productsRepository.remove(p.getId());
        }
        outputProductsIntoConsole(productsRepository.findAll(), "Find all product, after remove 'Селедочница'");

    }
}
