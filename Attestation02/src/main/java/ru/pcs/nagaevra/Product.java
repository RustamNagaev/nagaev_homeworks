package ru.pcs.nagaevra;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private int id;
    private String name;
    private String description;
    private double price;
    private int amount;
}
