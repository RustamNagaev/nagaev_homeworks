package ru.pcs.nagaevra;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import ru.pcs.nagaevra.ProductsRepository;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {
    //language=SQL
    private static final String SQL_INSERT = "insert into product (name, description, price, amount) values (?, ?, ?, ?)";
    private static final String SQL_UPDATE = "update product set name = ?, description = ?, price = ?, amount = ? where id = ?";
    private static final String SQL_DELETE = "delete from product where id = ?";
    private static final String SQL_SELECT_ALL = "select id, name, description, price, amount from product order by name";
    private static final String SQL_SELECT_BY_ID = "select id, name, description, price, amount from product where id = ?";
    private static final String SQL_SELECT_BY_LIKE_NAME = "select id, name, description, price, amount from product where lower(name) like lower('%'||?||'%') order by name";
    private static final String SQL_SELECT_ALL_BY_PRICE = "select id, name, description, price, amount from product where price = ? order by name";
    private static final String SQL_SELECT_ALL_BY_ORDERS_COUNT = "" +
            "select p.id, p.name, p.description, p.price, p.amount " +
            "from product p " +
            "where ? = (select count(*) from order_of_product o where o.product_id = p.id) " +
            "order by name";

    private final JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNum) -> {
        int id = row.getInt("id");
        String name = row.getString("name");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int amount = row.getInt("amount");

        return new Product(id, name, description, price, amount);
    };

    private static final ResultSetExtractor<Product> productExtractor = rs -> {
        Product product = null;
        if (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String description = rs.getString("description");
            double price = rs.getDouble("price");
            int amount = rs.getInt("amount");

            product = new Product(id, name, description, price, amount);
        }
        return product;
    };

    @Override
    public void add(Product p) {
        jdbcTemplate.update(SQL_INSERT, p.getName(), p.getDescription(), p.getPrice(), p.getAmount());
    }

    @Override
    public void update(Product p) {
        jdbcTemplate.update(SQL_UPDATE, p.getName(), p.getDescription(), p.getPrice(), p.getAmount(), p.getId());
    }

    @Override
    public void remove(int id) {
        jdbcTemplate.update(SQL_DELETE, id);
    }

    @Override
    public Product findById(int id) {
        Product product = jdbcTemplate.query(SQL_SELECT_BY_ID, productExtractor, id);
        if (product == null)
            throw new RuntimeException(String.format("Product with id=%s not found.", id));
        return product;
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByName(String name) {
        return jdbcTemplate.query(SQL_SELECT_BY_LIKE_NAME, productRowMapper, name);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_ORDERS_COUNT, productRowMapper, ordersCount);
    }
}
