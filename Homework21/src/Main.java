/*
Реализовать многопоточное суммирование элементов.

Отказался от, предложенного Марселем,
public class SumThread extends Thread,
т.к. он слегка утяжелял реализацию :).

 */

import java.util.*;

/**
 * 20.11.2021
 * 22. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static int[] array;

    public static int[] sums;

    public static volatile int sum;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();
        threadsCount = Math.max(threadsCount, 1);

        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int j : array) {
            realSum += j;
        }

        // для 2 000 000 -> 98996497, 98913187
        System.out.println(realSum);

        //=============================================================================
        // работа с потоками
        //=============================================================================
        List<Thread> threads = getListOfSumThreads(numbersCount, threadsCount);
        for (Thread thread: threads) {
            try {
                thread.start();
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }
        //=============================================================================

        int byThreadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }

        System.out.println(byThreadSum);

        System.out.println(sum);
    }

    private static List<Thread> getListOfSumThreads(int numbersCount, int threadsCount) {
        List<Thread> list = new ArrayList();

        int numbersPerThread = numbersCount / threadsCount;
        int remainder = numbersCount % threadsCount;

        threadsCount--;

        for (int i = 0; i <= (threadsCount); i++) {

            int from = i * numbersPerThread;
            int to = from + numbersPerThread - 1 + (i < threadsCount ? 0 : remainder);

            int finalI = i;
            list.add(new Thread(() -> {
                for (int j = from; j <= to; j++) {
                    Main.sums[finalI] += Main.array[j];
                    Main.sum += Main.array[j];
                }
            }));
        }

        return list;
    }
}
