import java.util.List;

public interface UsersRepository {

    void add(User user) throws IdIsOccupiedException;
    void update(User user);
    void remove(int id);

    int newId();
    User findById(int id);

    List<User> findAll();
    List<User> findByAge(int age);
    List<User> findByIsWorkerIsTrue();

}
