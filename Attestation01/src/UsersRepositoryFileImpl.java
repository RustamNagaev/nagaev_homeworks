import java.io.*;
import java.util.*;
import java.text.ParseException;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;
    private final String delimiter = "|";

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    private List<User> read() {
        List<User> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine())
                if (!line.isBlank())
                    lines.add(User.parseUser(line, "\\" + delimiter));
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }

    private void save(List<User> users, boolean append) {
        try (Writer writer = new FileWriter(fileName, append);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            for (User user : users) {
                bufferedWriter.write(user.asDelimitedString(delimiter));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int newId() {
        List<User> users = read();
        User user = Collections.max(users, Comparator.comparingInt(User::getId));
        return user.getId() + 1;
    }

    @Override
    public void add(User user) throws IdIsOccupiedException {
        try {
            findById(user.getId());
            throw new IdIsOccupiedException(String.format("Id=%s is occupied by another user", user.getId()));
        } catch (NoSuchElementException e) {
            save(List.of(user), true);
        }
    }

    @Override
    public void update(User user) {
        List<User> users = read();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == user.getId()) {
                users.set(i, user);
                save(users, false);
                return;
            }
        }
    }

    @Override
    public void remove(int id) {
        List<User> lines = read();
        Iterator<User> iterator = lines.listIterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId() == id) {
                iterator.remove();
                save(lines, false);
                return;
            }
        }
    }

    @Override
    public User findById(int id) {
        List<User> users = read();
        return users.stream()
                .filter(u -> u.getId() == id)
                .findFirst()
                .get();
    }

    @Override
    public List<User> findAll() {
        return read();
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = read();
        return users.stream()
                .filter(user -> user.getAge() == age)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = read();
        return users.stream()
                .filter(User::isWorker)
                .collect(Collectors.toList());
    }
}
