public class IdIsOccupiedException extends Exception {
    public IdIsOccupiedException(String s) {
        super(s);
    }
}
