import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        System.out.println("\nBefore update \n------------------------------");
        outputUsersIntoConsole(usersRepository.findAll());

        User user = usersRepository.findById(1);
        user.setName("Марсель");
        user.setAge(27);
        usersRepository.update(user);

        System.out.println("\nAfter update \n------------------------------");
        outputUsersIntoConsole(usersRepository.findAll());

        // to run additional tests, uncomment the line below
        // additionalTests(usersRepository);

        // return to the previous state :).
        user = usersRepository.findById(1);
        user.setName("Игорь");
        user.setAge(33);
        usersRepository.update(user);
    }

    private static void outputUsersIntoConsole(List<User> users) {
        for (User user : users) {
            System.out.println(user.getId() + " " + user.getName() + " " + user.getAge() + " " + user.isWorker());
        }
    }

    private static void additionalTests(UsersRepository usersRepository) {
        int newId = usersRepository.newId();
        try {
            usersRepository.add(new User(newId, "Сергей", 22, true));
        } catch (IdIsOccupiedException e) {
            System.err.println(e);
        }
        System.out.println("\nAfter adding Сергей \n------------------------------");
        outputUsersIntoConsole(usersRepository.findAll());

        System.out.println("\nAll working users \n------------------------------");
        outputUsersIntoConsole(usersRepository.findByIsWorkerIsTrue());

        System.out.println("\nAll who are 22 years old \n------------------------------");
        outputUsersIntoConsole(usersRepository.findByAge(22));

        usersRepository.remove(newId);
        System.out.println("\nAfter remove Сергей \n------------------------------");
        outputUsersIntoConsole(usersRepository.findAll());
    }
}
