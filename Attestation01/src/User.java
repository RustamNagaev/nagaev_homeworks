import java.text.ParseException;

public class User {
    private final int id;
    private String name;
    private int age;
    private boolean isWorker;

    public User(int id, String name, int age, boolean isWorker) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    public String asDelimitedString(String delimiter) {
        return new StringBuilder()
                .append(getId()).append(delimiter)
                .append(getName()).append(delimiter)
                .append(getAge()).append(delimiter)
                .append(isWorker()).toString();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }

    public static User parseUser(String str, String regex) throws ParseException {
        String[] fields = str.split(regex);
        try {
            if (fields.length == 4) {
                return new User(Integer.parseInt(fields[0])
                        , fields[1]
                        , Integer.parseInt(fields[2])
                        , Boolean.parseBoolean(fields[3]));
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new ParseException(String.format("row '%s' is not correct!",str), 0);
        }
    }
}
