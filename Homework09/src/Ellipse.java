public class Ellipse extends Figure {

    protected double radius;
    private double radius2;

    public Ellipse(int x, int y, double radius, double radius2) {
        super(x, y);
        this.radius = radius;
        this.radius2 = radius2;
    }

    @Override
    public double getPerimeter() {
        // примерный периметр эллипса
        return 4*((Math.PI* radius * radius2 + Math.pow((radius - radius2),2))/(radius + radius2));
    }

    @Override
    public String toString() {
        return "Ellipse{" +
                "x=" + x +
                ", y=" + y +
                ", radius=" + radius +
                ", radius2=" + radius2 +
                '}';
    }
}
