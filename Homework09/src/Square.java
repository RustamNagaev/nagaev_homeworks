public class Square extends Rectangle {

    public Square(int x, int y, double width) {
        super(x, y, width, width);
    }

    @Override
    public double getPerimeter() {
        return width*4;
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                '}';
    }
}
