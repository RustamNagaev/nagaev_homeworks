public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(1,1, 4,7);
        System.out.println(ellipse);
        System.out.println("Периметр эллипса: " + ellipse.getPerimeter());
        System.out.println();


        Circle circle = new Circle(10,10, 4);
        System.out.println(circle);
        System.out.println("Периметр круга: " + circle.getPerimeter());
        System.out.println();

        Rectangle rectangle = new Rectangle(1, 10, 4, 7);
        System.out.println(rectangle);
        System.out.println("Периметр прямоугольника: " + rectangle.getPerimeter());
        System.out.println();

        Square square = new Square(10, 1, 4);
        System.out.println(square);
        System.out.println("Периметр квадрата: " + square.getPerimeter());
    }

}
