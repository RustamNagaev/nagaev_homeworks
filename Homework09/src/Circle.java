public class Circle extends Ellipse {

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                ", radius=" + radius +
                '}';
    }
}
