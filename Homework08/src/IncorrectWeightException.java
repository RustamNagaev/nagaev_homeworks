public class IncorrectWeightException extends Exception {

    // конструктор класса
    public IncorrectWeightException(String message) {
        // вызов конструктора супер класса - Exception
        super(message);
    }

}
