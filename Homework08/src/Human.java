/**
 * Класс описывающий человека.
 * Может хранить имя человека и его вес
 */
public class Human {

    // поля характеризующие человека
    private String name;
    private double weight;

    /**
     * Получить имя
     * @return возвращает значение типа String
     */
    public String getName() {
        return name;
    }

    /**
     * Присвоить имя
     * @param name значение типа String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Получить вес
     * @return возвращает значение типа double
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Присвоить вес
     * @param weight значение типа double
     */
    public void setWeight(double weight) throws IncorrectWeightException {
        // если вес отрицательный выкинем IncorrectWeightException
        if (weight < 0.000)
            throw new IncorrectWeightException("Вес не может быть отрицательный!!!");
        this.weight = weight;
    }

    /**
     * Преобразовывает объект типа Human в строковое представление.
     * Например: Human{name='Васильев Олег', weight=45.446}
     * @return возвращает значение типа String
     */
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }

}

