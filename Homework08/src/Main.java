import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IncorrectWeightException {

        Scanner scanner = new Scanner(System.in);

        // объявление массива на 10 людей
        Human[] arrHuman = new Human[10];
        // наполнение массива
        System.out.println("Введите имена и веса 10 людей");
        for (int i = 0; i < 10; i++) {

            System.out.println((i+1) + " -------------------------------");

            String humanName = "";
            // т.к. имя не может быть пустым, сделаем цикл с проверкой на пустое имя
            while (humanName.length() == 0) {
                System.out.print("Имя: ");
                humanName = scanner.nextLine();
                if (humanName.length() == 0) {
                    System.out.println("Имя не может быть пустым! Укажите имя.");
                }
            }

            double humanWeight = 0;
            // т.к. вес не может быть нулевым и отрицательным, сделаем цикл с проверкой
            while (humanWeight == 0) {
                System.out.print("Вес, кг: ");
                // блок try...catch, для отлова некорректно введенного числа
                try {
                    humanWeight = Double.parseDouble(scanner.nextLine());
                    // проверка на отрицательный и нулевой вес
                    if (humanWeight <= 0) {
                        System.out.println("Вес не может быть отрицательным или равным нулю!");
                        humanWeight = 0;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Введен не корректный вес. Введите вес в килограммах, после запятой можно указать граммы");
                }

            }

            arrHuman[i] = new Human();
            arrHuman[i].setName(humanName);
            arrHuman[i].setWeight(humanWeight);
        }

        // выведем наполненный массив в консоль
        System.out.println();
        System.out.println(Arrays.toString(arrHuman));

        // отсортируем массив
        quickSortHumanByWeight(arrHuman);
        //selectSortHumanByWeight(arrHuman);

        // выведем отсортированный массив в консоль
        System.out.println(Arrays.toString(arrHuman));

    }

    /**
     * Быстрая сортировка массива объектов Human, по возрастанию веса (weight)
     * @param humans массив объектов Human
     */
    public static void quickSortHumanByWeight(Human[] humans) {
        if (humans.length >= 2) {

            int midElementIndex = (humans.length - 1) / 2;
            Human midElement = humans[midElementIndex];

            Human[] arrLeftTemp = new Human[humans.length];
            Human[] arrRightTemp = new Human[humans.length];
            int lastLeftIndex = 0, lastRightIndex = 0;

            for (int i = 0; i < humans.length; i++) {
                if (i != midElementIndex) {
                    if (humans[i].getWeight() < midElement.getWeight()) {
                        arrLeftTemp[lastLeftIndex] = humans[i];
                        lastLeftIndex++;
                    } else if (humans[i].getWeight() >= midElement.getWeight()) {
                        arrRightTemp[lastRightIndex] = humans[i];
                        lastRightIndex++;
                    }
                }
            }

            Human[] arrLeft = new Human[lastLeftIndex];
            for (int i = 0; i < lastLeftIndex; i++)
                arrLeft[i] = arrLeftTemp[i];
            quickSortHumanByWeight(arrLeft);

            Human[] arrRight = new Human[lastRightIndex];
            for (int i = 0; i < lastRightIndex; i++)
                arrRight[i] = arrRightTemp[i];
            quickSortHumanByWeight(arrRight);

            for (int i = 0; i < lastLeftIndex; i++)
                humans[i] = arrLeft[i];

            humans[lastLeftIndex] = midElement;

            for (int i = 0; i < lastRightIndex; i++)
                humans[lastLeftIndex + i + 1] = arrRight[i];

        }
    }

    /**
     * Сортировка массива объектов Human, по возрастанию веса (weight)
     * @param humans массив объектов Human
     * @throws IncorrectWeightException если в массиве будет обнаружен отрицательный или нулевой вес
     */
    public static void selectSortHumanByWeight(Human[] humans) throws IncorrectWeightException {
        for (int i = 0; i < humans.length; i++) {
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < humans[i].getWeight()) {
                    Human human = new Human();
                    human.setName(humans[i].getName());
                    human.setWeight(humans[i].getWeight());
                    humans[i] = humans[j];
                    humans[j] = human;
                }
            }
        }
    }

}

