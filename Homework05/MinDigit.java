import java.util.InputMismatchException;
import java.util.Scanner;

public class MinDigit {
    public static void main(String[] args) {
        // объект для считывания чисел с консоли
        Scanner scanner = new Scanner(System.in);
        // блок try для отлова ошибок, в случае считывания с консоли некорректного числа
        try {
            // объявление переменных и присваивание им начальных значений
            int minDigit = 10, intValue = 0;
            // цикл, пока не встретим -1
            while (intValue != -1) {
                // цикл, пока число intValue не станет равно нулю
                while (intValue != 0) {
                    // получаем последнюю цифру числа,
                    // в виде остатка от деления на 10
                    int digit = intValue % 10;
                    // если полученная цифра меньше значения переменной minDigit
                    if (minDigit > digit) {
                        // переменной minDigit присваиваем цифру
                        minDigit = digit;
                    }
                    // отбрасываем от исходного числа последнюю цифру
                    intValue = intValue / 10;
                }
                // на каждом шаге цикла считываем новое число из консоли
                intValue = scanner.nextInt();
                // по условиям задачи числовой ряд должен быть положительным, исключение число -1
                // по этому в случае числа < -1, выкинем InputMismatchException
                if (intValue < -1) {
                    throw new InputMismatchException();
                }
            }
            // во избежание вывода некорректного результата,
            // возникающего при считывании с консоли первым числом -1 (минус один)
            // сделаем проверку на начальное значение присвоенное переменной minDigit
            if (minDigit < 10) {
                // вывод результат в консоль
                System.out.println("Минимальная цифра: " + minDigit);
            }
        } catch (InputMismatchException e) {
            // Если поймали ошибку возникшую в результате считывания с консоли некорректного числа, выведем сообщение
            System.out.println("Введено не корректное число!");
            System.out.println("Число должно содержать цифры от 0 до 9 и принадлежать диапазону от -1 до 2147483647.");
        }
    }
}
