//  Написать модульный тест на int gcd()

package ru.pcs.nagaevra;

public class NumbersUtil {

    public boolean isPrime(int number) {

        if (number == 0 || number == 1)
            throw new IllegalArgumentException();

        if (number == 2 || number == 3)
            return true;

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0)
                return false;
        }

        return true;
    }

    public int sum(int a, int b) {
        return a + b;
    }

    /*
    Написать модульный тест на int gcd():

     нод(18, 12) -> 6
     нод(9, 12) -> 3
     нод(64, 48) -> 16

     Предусмотреть, когда на вход "некрсивые числа", отрицательные числа -> исключения
    */

    public int gcd(int a, int b) {

        if (a < 1 || b < 1)
            throw new IllegalArgumentException();

        while (a != b) {

            if (a > b)
                a -= b;

            if (b > a)
                b -= a;

        }
        return a;
    }
}
