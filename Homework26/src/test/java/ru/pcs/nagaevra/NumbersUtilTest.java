package ru.pcs.nagaevra;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    // то, что мы будем тестировать
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("isPrime() is working")
    public class ForIsPrime {
        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 71, 113})
        public void on_prime_numbers_return_true(int number) {
            assertTrue(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {22, 33, 72, 114})
        public void on_not_prime_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 169})
        public void on_sqr_numbers_return_false(int sqrNumber) {
            assertFalse(numbersUtil.isPrime(sqrNumber));
        }

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0, 1})
        public void bad_numbers_throws_exception(int badNumber) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.isPrime(badNumber));
        }
    }

    @Nested
    @DisplayName("sum() is working")
    public class ForSum {
        @ParameterizedTest(name = "return {2} on {0} + {1}")
        @CsvSource(value = {"5, 10, 15", "4, 2, 6", "11, -2, 9"})
        public void return_correct_sum(int a, int b, int result) {
            assertEquals(result, numbersUtil.sum(a, b));
        }
    }

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {
        @ParameterizedTest(name = "return gcd {2} for numbers: {0}, {1}")
        @CsvSource(value = {"8, 12, 4", "9, 12, 3", "64, 48, 16"})
        public void return_correct_gcd(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception for numbers: {0}, {1}")
        @CsvSource(value = {"-4, 2", "4, -2", "-4, -2", "0, 0"})
        public void bad_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class,() -> numbersUtil.gcd(a, b));
        }

    }
}
