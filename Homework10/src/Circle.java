public class Circle extends Ellipse implements MoveFigure {

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + this.getX() +
                ", y=" + this.getY() +
                ", radius=" + this.getRadius() +
                '}';
    }

    @Override
    public void setNewCoordinate(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

}
