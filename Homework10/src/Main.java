public class Main {
    public static void main(String[] args) {

        // Объявление и инициализация массива
        // для хранения "перемещаемых" фигур
        MoveFigure[] moveFigures = new MoveFigure[7];

        // Наполнение массива
        for (int i = 0; i < moveFigures.length; i++) {
            // генерация координат
            int x = (int) (100 * Math.random());
            int y = (int) (100 * Math.random());
            if (i < (moveFigures.length/2)) {
                // сначала наполним массив кругами
                moveFigures[i] = new Circle(x, y, (int) (9 * Math.random())+1);
            } else {
                // после наполним квадратами
                moveFigures[i] = new Square(x, y, (int) (9 * Math.random())+1);
                
            }
        }

        // выведем инфу по фигурам в консоль
        System.out.println();
        System.out.println("================= до перемещения =================");
        for (MoveFigure figure: moveFigures)
            System.out.println(figure);

        // Перемещение фигур в точку x=50, y=50
        for (MoveFigure figure: moveFigures)
            figure.setNewCoordinate(50,50);

        // Результат перемещения
        System.out.println();
        System.out.println("=============== после перемещения ================");
        for (MoveFigure figure: moveFigures)
            System.out.println(figure);


    }

}
