public class Square extends Rectangle implements MoveFigure {

    public Square(int x, int y, double width) {
        super(x, y, width, width);
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + this.getX() +
                ", y=" + this.getY() +
                ", width=" + this.getWidth() +
                '}';
    }

    @Override
    public void setNewCoordinate(int x, int y) {
        this.setX(x);
        this.setY(y);
    }
}
