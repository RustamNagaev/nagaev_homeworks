public class Rectangle extends Figure{

    private double width;
    private double height;

    public Rectangle(int x, int y, double width, double height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getPerimeter() {
        return width * 2 + height * 2;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + this.getX() +
                ", y=" + this.getY() +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
