import java.util.Arrays;

public class Main {

    /**
     * Функция возвращает первый найденный индекс числа в массиве.
     *
     * @param array массив целых чисел
     * @param number число, индекс которого требуется получить
     * @return возвращает первый найденный индекс числа в массиве, если число в массиве не обнаружено возвращает -1
     */
    public static int getElementIndex(int[] array, int number) {
        // цикл по массиву
        for (int i = 0; i < array.length; i++)
            // если i'ый элемент массива неравен искомому числу
            if (array[i] == number) {
                // прекратим выполнение функции и вернем индекс найденного числа
                return i;
            }
        // т.к. индекс искомого числа не был найден вернем -1
        return -1;
    }

    /**
     * Метод переносит все значимые элементы влево, заполнив нулевые.
     *
     * Например,
     *  было: 3, 0, 0, 4, 1, 0, 2, 0, 0, 8
     * стало: 3, 4, 1, 2, 8, 0, 0, 0, 0, 0
     *
     * @param array массив целых чисел
     */
    public static void moveElementsToLeft(int[] array) {
        // цикл по массиву
        for (int i = 0; i < array.length; i++) {
            // если i'ый элемент массива неравен нулю
            if (array[i] != 0) {
                // получим первый найденный индекс нуля в массиве
                int iZero = getElementIndex(array, 0);
                // если индекс нуля меньше текущего индекса массива
                if (iZero < i) {
                    // элементу массива с индексом нуля
                    // присвоим текущий элемент
                    array[iZero] = array[i];
                    // а текущему элементу присвоим ноль
                    array[i] = 0;
                }
            }
        }
    }

    public static void main(String[] args) {

        int[] arr = {123, 234, 345, 456, 0, 0, 567, 678, 0, 35, 76, 0, 989, 0, 54, 23, 76, 89, 0};

        System.out.println(getElementIndex(arr, 888));
        System.out.println(getElementIndex(arr, 567));

        System.out.println(Arrays.toString(arr));
        moveElementsToLeft(arr);
        System.out.println(Arrays.toString(arr));
    }
}
