/*
Реализовать в классе Sequence метод:
public static int[] filter(int[] array, ByCondition condition) {
	...
}
Данный метод возвращает массив, который содержит элементы, удовлетворяющие логическому выражению в condition.
*/

import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int i = 0;
        int[] tmpArray = new int[array.length];
        for (int val: array) {
            if (condition.isOk(val)) {
                tmpArray[i] = val;
                i++;
            }
        }
        return Arrays.copyOf(tmpArray,i);
    }
}
