/*
Предусмотреть функциональный интерфейс
interface ByCondition {
	boolean isOk(int number);
}
*/

public interface ByCondition {
    boolean isOk(int number);
}
