/*
В main в качестве condition подставить:
   - проверку на четность элемента
   - проверку, является ли сумма цифр элемента четным числом.
*/

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {0, 11, 23, 31, 43, 54, 65, 76, 87, 98, 101};

        // проверка на четность через анонимный класс
        int[] anonEven = Sequence.filter(array, new ByCondition() {
            @Override
            public boolean isOk(int number) {
                return number % 2 == 0 ? true : false;
            }
        });
        System.out.println(Arrays.toString(anonEven));

        // проверка на четность через лямбда выражение
        int[] lambdaEven = Sequence.filter(array, n -> n % 2 == 0 ? true : false);
        System.out.println(Arrays.toString(lambdaEven));

        // проверка, является ли сумма цифр элемента четным числом
        // через лямбда выражение
        lambdaEven = Sequence.filter(array, n -> {
            int sum = 0;
            while (n != 0) {
                sum += n % 10;
                n /= 10;
            }
           return sum%2 == 0? true: false;
        });
        System.out.println(Arrays.toString(lambdaEven));

    }
}
