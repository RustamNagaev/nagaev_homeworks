package ru.pcs.nagaevra.employees.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pcs.nagaevra.employees.dtos.EmployeeDto;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.models.Employee;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.repositories.EmployeeRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeesRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeesRepository = employeeRepository;
    }

    public Employee getEmployee(Long id) {
        return employeesRepository.findById(id);

    }

    public EmployeeDto getEmployeeAsDto(Long id) {

        Employee employee = getEmployee(id);

        EmployeeDto.EmployeeDtoBuilder employeeDtoBuilder = EmployeeDto.builder()
                .id(employee.getId())
                .juridicalFaceId(employee.getJuridicalFace().getId())
                .departmentId(employee.getDepartment().getId())
                .positionId(employee.getPosition().getId())
                .name(employee.getName())
                .techStack(employee.getTechStack())
                .education(employee.getEducation())
                .experience(employee.getExperience())
                .dismissReason(employee.getDismissReason());
        if (employee.getBeginWork() != null) {
            employeeDtoBuilder.beginWork(employee.getBeginWork().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        } else {
            employeeDtoBuilder.beginWork("");
        }
        if (employee.getDismiss() != null) {
            employeeDtoBuilder.dismiss(employee.getDismiss().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        } else {
            employeeDtoBuilder.dismiss("");
        }

        return employeeDtoBuilder.build();
    }

    public List<Employee> findAll() {
        return employeesRepository.findAll();
    }

    public List<Employee> findAllByName(String searchName) {
        return employeesRepository.findAllByName(searchName);
    }

    public void save(EmployeeDto employeeDto) {

        Employee.EmployeeBuilder employeeBuilder = Employee.builder()
                .id(employeeDto.getId())
                .juridicalFace(JuridicalFace.builder().id(employeeDto.getJuridicalFaceId()).build())
                .department(Department.builder().id(employeeDto.getDepartmentId()).build())
                .position((Position.builder().id(employeeDto.getPositionId()).build()))
                .name(employeeDto.getName())
                .techStack(employeeDto.getTechStack())
                .experience(employeeDto.getExperience())
                .education(employeeDto.getEducation())
                .dismissReason(employeeDto.getDismissReason());

        if (employeeDto.getBeginWork() != null)
            if (!employeeDto.getBeginWork().isEmpty())
                employeeBuilder.beginWork(LocalDate.parse(employeeDto.getBeginWork()));

        if (employeeDto.getDismiss() != null)
            if (!employeeDto.getDismiss().isEmpty())
                employeeBuilder.dismiss(LocalDate.parse(employeeDto.getDismiss()));

        Employee employee = employeeBuilder.build();

        if (employee.getId() == null) {
            employeesRepository.add(employee);
        } else {
            employeesRepository.update(employee);
        }

    }

    public void delete(Long id) {
        employeesRepository.delete(id);
    }
}
