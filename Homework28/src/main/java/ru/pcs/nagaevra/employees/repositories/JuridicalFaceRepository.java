package ru.pcs.nagaevra.employees.repositories;

import ru.pcs.nagaevra.employees.models.JuridicalFace;

import java.util.List;

public interface JuridicalFaceRepository {
    JuridicalFace findById(Long id);

    List<JuridicalFace> findAll();
}
