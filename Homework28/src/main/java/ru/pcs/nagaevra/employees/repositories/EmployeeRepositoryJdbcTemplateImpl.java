package ru.pcs.nagaevra.employees.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.nagaevra.employees.models.Employee;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;

@Component
public class EmployeeRepositoryJdbcTemplateImpl implements EmployeeRepository {

    //language=SQL
    private static final String SQL_INSERT_EMPLOYEE = "" +
            "insert into employee (\n" +
            "    juridical_face_id, department_id, position_id, name, tech_stack, experience, begin_work, education, dismiss, dismiss_reason\n" +
            ") values (\n" +
            "    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?\n" +
            ")";
    private static final String SQL_UPDATE_EMPLOYEE = "" +
            "update employee set \n" +
            "    juridical_face_id = ?, department_id = ?, position_id = ?, name = ?, tech_stack = ?\n" +
            "  , experience = ?, begin_work = ?, education = ?, dismiss = ?, dismiss_reason = ? \n" +
            "where id = ?";
    private static final String SQL_DELETE_EMPLOYEE = "delete from employee where id = ?";
    private static final String SQL_SELECT_ALL_EMPLOYEE = "" +
            "select id, juridical_face_id, department_id, position_id, name, tech_stack, experience, begin_work, education, dismiss, dismiss_reason\n" +
            "from employee \n" +
            "order by name";
    private static final String SQL_SELECT_EMPLOYEE_BY_ID = "" +
            "select id, juridical_face_id, department_id, position_id, name, tech_stack, experience, begin_work, education, dismiss, dismiss_reason\n" +
            "from employee \n" +
            "where id = ?";
    private static final String SQL_SELECT_EMPLOYEE_BY_LIKE_NAME = "" +
            "select id, juridical_face_id, department_id, position_id, name, tech_stack, experience, begin_work, education, dismiss, dismiss_reason\n" +
            "from employee \n" +
            "where lower(name) like lower('%'||?||'%') order by name";

    private final JdbcTemplate jdbcTemplate;
    private static PositionRepository positionsRepository;
    private static JuridicalFaceRepository juridicalFaceRepository;
    private static DepartmentRepository departmentRepository;

    @Autowired
    public EmployeeRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        positionsRepository = new PositionRepositoryJdbcTemplateImpl(dataSource);
        juridicalFaceRepository = new JuridicalFaceRepositoryJdbcTemplateImpl(dataSource);
        departmentRepository = new DepartmentRepositoryJdbcTemplateImpl(dataSource);
    }

    private static final RowMapper<Employee> employeeRowMapper = (row, rowNum) -> {
        Long id = row.getLong("id");
        Long juridicalFaceId = row.getLong("juridical_face_id");
        Long departmentId = row.getLong("department_id");
        Long positionId = row.getLong("position_id");
        String name = row.getString("name");
        String techStack = row.getString("tech_stack");
        String experience = row.getString("experience");
        String education = row.getString("education");

        LocalDate beginWork = null;
        if (row.getDate("begin_work") != null)
            beginWork = row.getDate("begin_work").toLocalDate();

        LocalDate dismiss = null;
        if (row.getDate("dismiss") != null)
            dismiss = row.getDate("dismiss").toLocalDate();

        String dismissReason = row.getString("dismiss_reason");
        return new Employee(id
                , juridicalFaceRepository.findById(juridicalFaceId)
                , departmentRepository.findById(departmentId)
                , positionsRepository.findById(positionId)
                , name
                , techStack
                , experience
                , beginWork
                , education
                , dismiss
                , dismissReason
        );
    };

    @Override
    public void add(Employee p) {
        jdbcTemplate.update(SQL_INSERT_EMPLOYEE
                , p.getJuridicalFace().getId()
                , p.getDepartment().getId()
                , p.getPosition().getId()
                , p.getName()
                , p.getTechStack()
                , p.getExperience()
                , p.getBeginWork()
                , p.getEducation()
                , p.getDismiss()
                , p.getDismissReason()
        );
    }

    @Override
    public void update(Employee p) {
        jdbcTemplate.update(SQL_UPDATE_EMPLOYEE
                , p.getJuridicalFace().getId()
                , p.getDepartment().getId()
                , p.getPosition().getId()
                , p.getName()
                , p.getTechStack()
                , p.getExperience()
                , p.getBeginWork()
                , p.getEducation()
                , p.getDismiss()
                , p.getDismissReason()
                , p.getId()
        );
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(SQL_DELETE_EMPLOYEE, id);
    }

    @Override
    public Employee findById(Long id) {
        return jdbcTemplate.query(SQL_SELECT_EMPLOYEE_BY_ID, employeeRowMapper, id).get(0);
    }

    @Override
    public List<Employee> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_EMPLOYEE, employeeRowMapper);
    }

    @Override
    public List<Employee> findAllByName(String name) {
        return jdbcTemplate.query(SQL_SELECT_EMPLOYEE_BY_LIKE_NAME, employeeRowMapper, name);
    }
}
