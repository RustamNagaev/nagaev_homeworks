package ru.pcs.nagaevra.employees.repositories;

import ru.pcs.nagaevra.employees.models.Department;

import java.util.List;

public interface DepartmentRepository {
    Department findById(Long id);

    List<Department> findAll();
}
