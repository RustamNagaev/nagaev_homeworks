package ru.pcs.nagaevra.employees.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.nagaevra.employees.models.Position;

import javax.sql.DataSource;
import java.util.List;

@Component
public class PositionRepositoryJdbcTemplateImpl implements PositionRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select id, name from position order by name";
    private static final String SQL_SELECT_BY_ID = "select id, name from position where id = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PositionRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Position findById(Long id) {
        return jdbcTemplate.query(SQL_SELECT_BY_ID, positionRowMapper, id).get(0);
    }

    @Override
    public List<Position> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, positionRowMapper);
    }

    private static final RowMapper<Position> positionRowMapper = (row, rowNum) -> {
        Long id = row.getLong("id");
        String name = row.getString("name");
        return new Position(id, name);
    };

}