package ru.pcs.nagaevra.employees.repositories;

import ru.pcs.nagaevra.employees.models.Position;

import java.util.List;

public interface PositionRepository {

    Position findById(Long id);

    List<Position> findAll();
}
