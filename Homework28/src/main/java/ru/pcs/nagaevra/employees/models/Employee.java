package ru.pcs.nagaevra.employees.models;

import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Employee {
    private Long id;
    private JuridicalFace juridicalFace;
    private Department department;
    private Position position;
    private String name;
    private String techStack;
    private String experience;
    private LocalDate beginWork;
    private String education;
    private LocalDate dismiss;
    private String dismissReason;
}
