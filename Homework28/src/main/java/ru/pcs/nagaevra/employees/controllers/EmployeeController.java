package ru.pcs.nagaevra.employees.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.nagaevra.employees.dtos.EmployeeDto;
import ru.pcs.nagaevra.employees.models.Department;
import ru.pcs.nagaevra.employees.models.Employee;
import ru.pcs.nagaevra.employees.models.JuridicalFace;
import ru.pcs.nagaevra.employees.models.Position;
import ru.pcs.nagaevra.employees.services.DepartmentService;
import ru.pcs.nagaevra.employees.services.EmployeeService;
import ru.pcs.nagaevra.employees.services.JuridicalFaceService;
import ru.pcs.nagaevra.employees.services.PositionService;

import java.util.List;

@Controller
public class EmployeeController {

    private final EmployeeService employeesService;
    private final PositionService positionsService;
    private final JuridicalFaceService juridicalFacesService;
    private final DepartmentService departmentsService;

    @Autowired
    public EmployeeController(EmployeeService employeesService
            , PositionService positionsService
            , JuridicalFaceService juridicalFacesService
            , DepartmentService departmentsService) {

        this.employeesService = employeesService;
        this.positionsService = positionsService;
        this.juridicalFacesService = juridicalFacesService;
        this.departmentsService = departmentsService;
    }

    @GetMapping("/list-employee")
    public String getEmployeesPage(Model model) {
        listOfEmployee(model, "");
        return "list-employee";
    }

    @GetMapping("/search-employee")
    public String getEmployeePage(Model model, @RequestParam("searchName") String searchName) {
        listOfEmployee(model, searchName);
        return "list-employee";
    }

    private void listOfEmployee(Model model, String searchName) {
        List<Employee> employees;
        if (searchName.isEmpty())  {
            employees = employeesService.findAll();
        } else {
            employees = employeesService.findAllByName(searchName);
        }
        model.addAttribute("searchName", searchName);
        model.addAttribute("employees", employees);
    }

    @GetMapping("/add-employee")
    public String getAddEmployeePage(Model model) {

        List<Position> positions = positionsService.findAll();
        List<JuridicalFace> juridicalFaces = juridicalFacesService.findAll();
        List<Department> departments = departmentsService.findAll();

        model.addAttribute("positions", positions);
        model.addAttribute("juridicalFaces", juridicalFaces);
        model.addAttribute("departments", departments);

        return "add-employee";
    }

    @PostMapping("/add-employee")
    public String addEmployee(EmployeeDto employeeDto) {

        employeesService.save(employeeDto);

        return "redirect:/list-employee";
    }


    @GetMapping("/list-employee/{employee-id}/detail-employee")
    public String getEmployeePage(Model model, @PathVariable("employee-id") Long employeeId) {

        EmployeeDto employee = employeesService.getEmployeeAsDto(employeeId);

        List<Position> positions = positionsService.findAll();
        List<JuridicalFace> juridicalFaces = juridicalFacesService.findAll();
        List<Department> departments = departmentsService.findAll();

        model.addAttribute("employee", employee);
        model.addAttribute("positions", positions);
        model.addAttribute("juridicalFaces", juridicalFaces);
        model.addAttribute("departments", departments);

        return "detail-employee";
    }

    @PostMapping("/list-employee/{employee-id}/update-employee")
    public String updateEmployee(EmployeeDto employeeDto, @PathVariable("employee-id") Long employeeId) {

        employeeDto.setId(employeeId);

        employeesService.save(employeeDto);

        return "redirect:/list-employee";
    }

    @PostMapping("/list-employee/{employee-id}/delete-employee")
    public String deleteEmployee(EmployeeDto employeeDto, @PathVariable("employee-id") Long employeeId) {

        employeesService.delete(employeeId);

        return "redirect:/list-employee";
    }

}
