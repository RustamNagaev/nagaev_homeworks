/*
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

o001aa111|Toyota Camry|Yellow|132|720000
o002aa111|Toyota Camry|Green|133|830000
o003aa111|Toyota Camry|Black|134|850000
o003aa111|Toyota Camry|Black|134|850000
o004aa111|VW Touareg|White|0|1020000
o005aa111|VW Touareg|Blue|235|790000
o005aa111|VW Touareg|Blue|235|790000

Используя Java Stream API, вывести:

Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry *

Достаточно сделать один из вариантов.
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        String fileName = "cars.txt";

        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег:");
        try (BufferedReader reader = getBufferedFileReader(fileName)) {
            getStreamWithDistinctCars(reader)
                    .filter(car -> car.getColor().equalsIgnoreCase("black") || car.getMileage() == 0)
                    .forEach(car -> System.out.println(car.getNumber()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.print("\nКоличество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.: ");
        try (BufferedReader reader = getBufferedFileReader(fileName)) {
            long count = getStreamWithDistinctCars(reader)
                    .filter(car -> car.getPrice() >= 700_000 && car.getPrice() < 800_000)
                    .count();
            System.out.println(count);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.print("\nЦвет автомобиля с минимальной стоимостью: ");
        try (BufferedReader reader = getBufferedFileReader(fileName)) {
            String color = getStreamWithDistinctCars(reader)
                    .min(Comparator.comparingDouble(Car::getPrice))
                    .get().getColor();
            System.out.println(color);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.print("\nСредняя стоимость Toyota Camry: ");
        try (BufferedReader reader = getBufferedFileReader(fileName)) {
            double average =  getStreamWithDistinctCars(reader)
                    .filter(car -> car.getModel().equalsIgnoreCase("toyota camry"))
                    .mapToDouble(Car::getPrice)
                    .average().getAsDouble();
            System.out.println(average);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
    private static BufferedReader getBufferedFileReader(String fileName) throws FileNotFoundException {
        return new BufferedReader(new FileReader(fileName));
    }

    private static Stream<Car> getStreamWithDistinctCars(BufferedReader reader) {
        return reader.lines()
                .distinct()
                .map(line -> line.split("\\|"))
                .map(arr -> new Car(arr[0], arr[1], arr[2], Integer.parseInt(arr[3]), Double.parseDouble(arr[4])));
    }
}
